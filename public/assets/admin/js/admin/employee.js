/*
var Employee = (function () {
  var handleTables = function () {
    var table = $('#employee').DataTable({
      "destroy": true,
      "responsive": true,
      "serverSide": true,
      "processing": true,
      "ajax": {
        "url" : rurl + 'admin/employee/list',
        "type" : "POST",
        "data" : {
          "companies" : $('#companies').val(),
          "branches" : $('#branches').val(),
          "levels" : $('#levels').val(),
          "groups" : $('#groups').val(),
          "departments" : $('#departments').val(),
          "employee_status_id" : $('#employeestatus').val(),
          "hospital_id" : $('#hospital').val(),
          "employee_level_id":$('#employeelevel').val(),
        },
      },

      "language": {
        "url": rurl + "assets/plugins/datatable_th.json"
      },
      "columns": [{
          "data": 'DT_RowIndex',
          "name": 'DT_RowIndex',
          orderable: false,
          searchable: false,
          className: "text-center"
        },
        {"data": "picture_profile","name": "employee.picture_profile"},
        {"data": "empcode","name": "employee.empcode"},
        {"data": "cid","name": "employee.cid"},
        {"data": "prename","name": "employee.prename"},
        {"data": "firstname","name": "employee.firstname"},
        {"data": "lastname","name": "employee.lastname"},
        {"data": "prename_en","name": "employee.prename_en" ,visible:false},
        {"data": "firstname_en","name": "employee.firstname_en" ,visible:false},
        {"data": "lastname_en","name": "employee.lastname_en" ,visible:false},
        {"data": "nickname","name": "employee.nickname" ,visible:false},
        {"data": "mobile","name": "employee.mobile" ,visible:false},
        {"data":"lname","name":"level.name"},
        {"data":"dname","name":"department.name"},
        {"data":"gname","name":"groups.name"},
        {"data": "branch_name","name": "branch.branch_name"},
        {"data":"ename","name":"employee_level.name" ,visible:false},
        {"data": "gender","name": "employee.gender",visible:false},
        {"data":"birthday","name":"employee.birthday" ,visible:false},
        
        {"data":"age" , searchable: false ,orderable:false ,visible:false},
        {"data":"startworking_date" , "name":"employee.startworking_date" ,visible:false},
        {"data":"workage" , searchable: false , orderable:false ,visible:false},
        {"data":"workage_month" , searchable: false , orderable:false ,visible:false},
        {"data": "email","name": "employee.email" ,visible:false},
        {"data":"cname","name":"company.name" ,visible:false},
        {"data":"hname","name":"hospitals.name" ,visible:false},
        {"data":"esname","name":"employee_status.name"},
        {"data":"employee_leave_approver",searchable: false ,visible:false},
        {"data":"address",searchable: false ,visible:false},
        {"data":"address_cr",searchable: false ,visible:false},
        {"data":"skill_description",searchable: false ,visible:false},
        {"data":"authority",searchable: false ,visible:false},
        {"data":"remark","name":"employee.remark" ,visible:false},
        {
          "data": "action",
          orderable: false,
          searchable: false
        }
      ],
      "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      buttons: [
        {
          extend: 'print',
          className: 'btn btn-default btn-sm',
          text: '<i class="fas fa-print"></i> Print',
          orientation: 'landscape',
          title:'ข้อมูลพนักงาน',
        },
        {
          extend: 'excel',
          className: 'btn btn-default btn-sm',
          text: '<i class="fas fa-file-excel"></i> Excel',
          orientation: 'landscape',
          title:'ข้อมูลพนักงาน',
        },
      ],
    });
  }

  var handleButton = function () {
    $(document).on('click', '.btn-delete', function () {
      $this = $(this)
      swal({
          title: "คุณแน่ใจไหม?",
          text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "ใช่, ยืนยัน!",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false
        },
        function () {
          deleteUser($this);
        });
    });

    $(document).on('click', '.btn-edit', function () {
      $(this).closest('form').submit();
    });

    $(document).on('click', '.btn-print', function (e) {
      var this_url = $(this).attr('href');
      var filter = $('#filter').serialize();
      var new_url = this_url+'?'+filter;
      window.open(new_url);
      e.preventDefault();
    });

  }

  var deleteUser = function (value) {
    var id = value.data('id');
    var token = value.data('token');

    $.ajax({
      url: rurl + 'admin/employee/' + id,
      type: 'DELETE',
      data: {
        _method: 'delete',
        _token: token,
        _id: id
      },
      success: function (data) {
        $("#employee").DataTable().ajax.reload(null, false);
        swal('ยินดีด้วย!', data, "success");
        table.ajax.reload();
      },
      error: function (data) {
        swal("พบข้อผิดผลาด!", data, "error");
      }
    });
  }
  $('#companies').change(function (e) {
    handleTables();
  });
  $('#branches').change(function (e) {
    handleTables();
  });
  $('#groups').change(function (e) {
    handleTables();
  });
  $('#departments').change(function (e) {
    handleTables();
  });
  $('#levels').change(function (e) {
    handleTables();
  });
  $('#employeestatus').change(function (e) {
    handleTables();
  });
  $('#hospital').change(function (e) {
    handleTables();
  });

  $('#employeelevel').change(function (e) {
    handleTables();
  });

  return {
    // main function to initiate the module
    init: function () {
      handleTables();
      handleButton();
    }
  }
})()

jQuery(document).ready(function () {
  Employee.init();
})

*/


var Employee = (function () {
  var handleTables = function () {
    var option_table = {
      oLanguage: {sProcessing: "<div id='loader'></div>"},

      "destroy": true,
      "responsive": true,
      "serverSide": true,
      "processing": true,
      "ajax": {
        "url" : rurl + 'admin/employee/list',
        "type" : "POST",
        "data" : {
          "companies" : $('#companies').val(),
          "branches" : $('#branches').val(),
          "levels" : $('#levels').val(),
          "groups" : $('#groups').val(),
          "departments" : $('#departments').val(),
          "employee_status_id" : $('#employeestatus').val(),
          "hospital_id" : $('#hospital').val(),
          "employee_level_id":$('#employeelevel').val(),
        },
      },

      "language": {
        "url": rurl + "assets/plugins/datatable_th.json"
      },
      "columns": [{
          "data": 'DT_RowIndex',
          "name": 'DT_RowIndex',
          orderable: false,
          searchable: false,
          className: "text-center"
        },
        {"data": "picture_profile","name": "employee.picture_profile"},
        {"data": "empcode","name": "employee.empcode"},
        {"data": "cid","name": "employee.cid"},
        {"data": "prename","name": "employee.prename"},
        {"data": "firstname","name": "employee.firstname"},
        {"data": "lastname","name": "employee.lastname"},
        {"data": "prename_en","name": "employee.prename_en" ,visible:false},
        {"data": "firstname_en","name": "employee.firstname_en" ,visible:false},
        {"data": "lastname_en","name": "employee.lastname_en" ,visible:false},
        {"data": "nickname","name": "employee.nickname" ,visible:false},
        {"data": "mobile","name": "employee.mobile" ,visible:false},
        {"data":"lname","name":"level.name"},
        {"data":"dname","name":"department.name"},
        {"data":"gname","name":"groups.name"},
        {"data": "branch_name","name": "branch.branch_name"},
        {"data":"ename","name":"employee_level.name" ,visible:false},
        {"data": "gender","name": "employee.gender",visible:false},
        {"data":"birthday","name":"employee.birthday" ,visible:false},
        
        {"data":"age" , searchable: false ,orderable:false ,visible:false},
        {"data":"startworking_date" , "name":"employee.startworking_date" ,visible:false},
        {"data":"workage" , searchable: false , orderable:false ,visible:false},
        {"data":"workage_month" , searchable: false , orderable:false ,visible:false},
        {"data": "email","name": "employee.email" ,visible:false},
        {"data":"cname","name":"company.name" ,visible:false},
        {"data":"hname","name":"hospitals.name" ,visible:false},
        {"data":"esname","name":"employee_status.name"},
        {"data":"employee_leave_approver",searchable: false ,visible:false},
        {"data":"address",searchable: false ,visible:false},
        {"data":"address_cr",searchable: false ,visible:false},
        {"data":"skill_description",searchable: false ,visible:false},
        {"data":"authority",searchable: false ,visible:false},
        {"data":"remark","name":"employee.remark" ,visible:false},
        {
          "data": "action",
          orderable: false,
          searchable: false
        }
      ],
      "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      buttons: [
        {
          extend: 'print',
          className: 'btn btn-default btn-sm',
          text: '<i class="fas fa-print"></i> Print',
          orientation: 'landscape',
          title:'ข้อมูลพนักงาน',
        },
        {
          extend: 'excel',
          className: 'btn btn-default btn-sm',
          text: '<i class="fas fa-file-excel"></i> Excel',
          orientation: 'landscape',
          title:'ข้อมูลพนักงาน',
        },
      ],
    };
    var table = $('#employee').DataTable(option_table);

    $('form#filter').submit(function (e) {
      e.preventDefault();
      new_option = {
        oLanguage: {sProcessing: "<div id='loader'></div>"},

        "destroy": true,
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
          "url" : rurl + 'admin/employee/list',
          "type" : "POST",
          "data" : {
            "companies" : $('#companies').val(),
            "branches" : $('#branches').val(),
            "levels" : $('#levels').val(),
            "groups" : $('#groups').val(),
            "departments" : $('#departments').val(),
            "employee_status_id" : $('#employeestatus').val(),
            "hospital_id" : $('#hospital').val(),
            "employee_level_id":$('#employeelevel').val(),
          },
        },
  
        "language": {
          "url": rurl + "assets/plugins/datatable_th.json"
        },
        "columns": [{
            "data": 'DT_RowIndex',
            "name": 'DT_RowIndex',
            orderable: false,
            searchable: false,
            className: "text-center"
          },
          {"data": "picture_profile","name": "employee.picture_profile"},
          {"data": "empcode","name": "employee.empcode"},
          {"data": "cid","name": "employee.cid"},
          {"data": "prename","name": "employee.prename"},
          {"data": "firstname","name": "employee.firstname"},
          {"data": "lastname","name": "employee.lastname"},
          {"data": "prename_en","name": "employee.prename_en" ,visible:false},
          {"data": "firstname_en","name": "employee.firstname_en" ,visible:false},
          {"data": "lastname_en","name": "employee.lastname_en" ,visible:false},
          {"data": "nickname","name": "employee.nickname" ,visible:false},
          {"data": "mobile","name": "employee.mobile" ,visible:false},
          {"data":"lname","name":"level.name"},
          {"data":"dname","name":"department.name"},
          {"data":"gname","name":"groups.name"},
          {"data": "branch_name","name": "branch.branch_name"},
          {"data":"ename","name":"employee_level.name" ,visible:false},
          {"data": "gender","name": "employee.gender",visible:false},
          {"data":"birthday","name":"employee.birthday" ,visible:false},
          
          {"data":"age" , searchable: false ,orderable:false ,visible:false},
          {"data":"startworking_date" , "name":"employee.startworking_date" ,visible:false},
          {"data":"workage" , searchable: false , orderable:false ,visible:false},
          {"data":"workage_month" , searchable: false , orderable:false ,visible:false},
          {"data": "email","name": "employee.email" ,visible:false},
          {"data":"cname","name":"company.name" ,visible:false},
          {"data":"hname","name":"hospitals.name" ,visible:false},
          {"data":"esname","name":"employee_status.name"},
          {"data":"employee_leave_approver",searchable: false ,visible:false},
          {"data":"address",searchable: false ,visible:false},
          {"data":"address_cr",searchable: false ,visible:false},
          {"data":"skill_description",searchable: false ,visible:false},
          {"data":"authority",searchable: false ,visible:false},
          {"data":"remark","name":"employee.remark" ,visible:false},
          {
            "data": "action",
            orderable: false,
            searchable: false
          }
        ],
        "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        buttons: [
          {
            extend: 'print',
            className: 'btn btn-default btn-sm',
            text: '<i class="fas fa-print"></i> Print',
            orientation: 'landscape',
            title:'ข้อมูลพนักงาน',
          },
          {
            extend: 'excel',
            className: 'btn btn-default btn-sm',
            text: '<i class="fas fa-file-excel"></i> Excel',
            orientation: 'landscape',
            title:'ข้อมูลพนักงาน',
          },
        ],
      };
      $('#employee').DataTable().destroy();
      $('#employee').DataTable(new_option);
    });
  }
  

  var handleButton = function () {
    $(document).on('click', '.btn-delete', function () {
      $this = $(this)
      swal({
          title: "คุณแน่ใจไหม?",
          text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "ใช่, ยืนยัน!",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false
        },
        function () {
          deleteUser($this);
        });
    });

    $(document).on('click', '.btn-edit', function () {
      $(this).closest('form').submit();
    });

    $(document).on('click', '.btn-print', function (e) {
      var this_url = $(this).attr('href');
      var filter = $('#filter').serialize();
      var new_url = this_url+'?'+filter;
      window.open(new_url);
      e.preventDefault();
    });

  }

  var deleteUser = function (value) {
    var id = value.data('id');
    var token = value.data('token');

    $.ajax({
      url: rurl + 'admin/employee/' + id,
      type: 'DELETE',
      data: {
        _method: 'delete',
        _token: token,
        _id: id
      },
      success: function (data) {
        $("#employee").DataTable().ajax.reload(null, false);
        swal('ยินดีด้วย!', data, "success");
        table.ajax.reload();
      },
      error: function (data) {
        swal("พบข้อผิดผลาด!", data, "error");
      }
    });
  }

  /*======== บริษัท ===========*/
  $('#companies').multiselect({
    nonSelectedText: '== บริษัท ==',
    filterPlaceholder: 'ค้นหา',
    enableFiltering: true,
    includeSelectAllOption: true,
    selectAllText: '== เลือกทั้งหมด ==',
    maxHeight: 260,
    buttonWidth: '100%',
    buttonTextAlignment: 'left',
    dropUp: true,
    buttonText: function(options, select) {
      if (options.length == 0) {
        return '== บริษัท ==';
      }
      else {
        if (options.length > this.numberDisplayed) {
          return options.length + ' ' + this.nSelectedText;
        }
        else {
          var selected = '';
          options.each(function() {
            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
    
            selected += label + ', ';
          });
          return selected.substr(0, selected.length - 2);
        }
      }
    }
  });

  /*======== สาขา ===========*/
  $('#branches').multiselect({
    nonSelectedText: '== สาขา ==',
    filterPlaceholder: 'ค้นหา',
    enableFiltering: true,
    includeSelectAllOption: true,
    selectAllText: '== เลือกทั้งหมด ==',
    maxHeight: 260,
    buttonWidth: '100%',
    buttonTextAlignment: 'left',
    dropUp: true,
    buttonText: function(options, select) {
      if (options.length == 0) {
        return '== สาขา ==';
      }
      else {
        if (options.length > this.numberDisplayed) {
          return options.length + ' ' + this.nSelectedText;
        }
        else {
          var selected = '';
          options.each(function() {
            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
    
            selected += label + ', ';
          });
          return selected.substr(0, selected.length - 2);
        }
      }
    }
  });

  /*======== ฝ่าย ===========*/
  $('#groups').multiselect({
    nonSelectedText: '== ฝ่าย ==',
    filterPlaceholder: 'ค้นหา',
    enableFiltering: true,
    includeSelectAllOption: true,
    selectAllText: '== เลือกทั้งหมด ==',
    maxHeight: 260,
    buttonWidth: '100%',
    buttonTextAlignment: 'left',
    dropUp: true,
    buttonText: function(options, select) {
      if (options.length == 0) {
        return '== ฝ่าย ==';
      }
      else {
        if (options.length > this.numberDisplayed) {
          return options.length + ' ' + this.nSelectedText;
        }
        else {
          var selected = '';
          options.each(function() {
            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
    
            selected += label + ', ';
          });
          return selected.substr(0, selected.length - 2);
        }
      }
    }
  });

  /*======== แผนก ===========*/
  $('#departments').multiselect({
    nonSelectedText: '== แผนก ==',
    filterPlaceholder: 'ค้นหา',
    enableFiltering: true,
    includeSelectAllOption: true,
    selectAllText: '== เลือกทั้งหมด ==',
    maxHeight: 260,
    buttonWidth: '100%',
    buttonTextAlignment: 'left',
    dropUp: true,
    buttonText: function(options, select) {
      if (options.length == 0) {
        return '== แผนก ==';
      }
      else {
        if (options.length > this.numberDisplayed) {
          return options.length + ' ' + this.nSelectedText;
        }
        else {
          var selected = '';
          options.each(function() {
            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
    
            selected += label + ', ';
          });
          return selected.substr(0, selected.length - 2);
        }
      }
    }
  });

  /*======== ตำแหน่ง ===========*/
  $('#levels').multiselect({
    nonSelectedText: '== ตำแหน่ง ==',
    filterPlaceholder: 'ค้นหา',
    enableFiltering: true,
    includeSelectAllOption: true,
    selectAllText: '== เลือกทั้งหมด ==',
    maxHeight: 260,
    buttonWidth: '100%',
    buttonTextAlignment: 'left',
    dropUp: true,
    buttonText: function(options, select) {
      if (options.length == 0) {
        return '== ตำแหน่ง ==';
      }
      else {
        if (options.length > this.numberDisplayed) {
          return options.length + ' ' + this.nSelectedText;
        }
        else {
          var selected = '';
          options.each(function() {
            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
    
            selected += label + ', ';
          });
          return selected.substr(0, selected.length - 2);
        }
      }
    }
  });

  /*======== ระดับพนักงาน ===========*/
  $('#employeelevel').multiselect({
    nonSelectedText: '== ระดับพนักงาน ==',
    filterPlaceholder: 'ค้นหา',
    enableFiltering: true,
    includeSelectAllOption: true,
    selectAllText: '== เลือกทั้งหมด ==',
    maxHeight: 260,
    buttonWidth: '100%',
    buttonTextAlignment: 'left',
    dropUp: true,
    buttonText: function(options, select) {
      if (options.length == 0) {
        return '== ระดับพนักงาน ==';
      }
      else {
        if (options.length > this.numberDisplayed) {
          return options.length + ' ' + this.nSelectedText;
        }
        else {
          var selected = '';
          options.each(function() {
            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
    
            selected += label + ', ';
          });
          return selected.substr(0, selected.length - 2);
        }
      }
    }
  });

  /*======== ประกันสังคม ===========*/
  $('#hospital').multiselect({
    nonSelectedText: '== ประกันสังคม ==',
    filterPlaceholder: 'ค้นหา',
    enableFiltering: true,
    includeSelectAllOption: true,
    selectAllText: '== เลือกทั้งหมด ==',
    maxHeight: 260,
    buttonWidth: '100%',
    buttonTextAlignment: 'left',
    dropUp: true,
    buttonText: function(options, select) {
      if (options.length == 0) {
        return '== ประกันสังคม ==';
      }
      else {
        if (options.length > this.numberDisplayed) {
          return options.length + ' ' + this.nSelectedText;
        }
        else {
          var selected = '';
          options.each(function() {
            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
    
            selected += label + ', ';
          });
          return selected.substr(0, selected.length - 2);
        }
      }
    }
  });

  /*======== สถานะ ===========*/
  $('#employeestatus').multiselect({
    nonSelectedText: '== สถานะ ==',
    filterPlaceholder: 'ค้นหา',
    enableFiltering: true,
    includeSelectAllOption: true,
    selectAllText: '== เลือกทั้งหมด ==',
    maxHeight: 260,
    buttonWidth: '100%',
    buttonTextAlignment: 'left',
    dropUp: true,
    buttonText: function(options, select) {
      if (options.length == 0) {
        return '== สถานะ ==';
      }
      else {
        if (options.length > this.numberDisplayed) {
          return options.length + ' ' + this.nSelectedText;
        }
        else {
          var selected = '';
          options.each(function() {
            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
    
            selected += label + ', ';
          });
          return selected.substr(0, selected.length - 2);
        }
      }
    },
  });

  return {
    // main function to initiate the module
    init: function () {
      handleTables();
      handleButton();
    }
  }
})()

jQuery(document).ready(function () {
  Employee.init();
})