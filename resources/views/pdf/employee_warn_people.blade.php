{{-- <!DOCTYPE html> --}}
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงานการเตือน(รายบุคคล)</title>
    <style>
@font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body {
            font-family: "THSarabunNew";
            font-size: 16px;
        }
        @page {
            margin: 0.4in;
        }
        @media print {
            html,
            body {
                font-size : 16px;
            }
        }
        table {
            width: 100%;
            margin: 0px;
            padding: 0px;
            border-collapse: collapse;
        }
        tr , td , th {
            margin: 0px;
            text-align: center;
            border: border: 1px solid #8a8a8a;
            line-height: 12px;
            padding: 0px;
        }
        img.pictureprofile {
            width:80px;
            height:auto;
            /* border:solid 1px #8a8a8a;
            border-radius: 5px; */
        }
        .page_break { page-break-after: always; }
    </style>
</head>
<body>
    <center>
        <p style="font-size:18px; margin:0px;">
            รายงานผลการเตือน
            <br>
            @if(isset($request['date_start'])&&isset($request['date_end']))
            ตั้งแต่วันที่
            {{ App\Http\Controllers\FunctionController::DateThai($request['date_start']) }} 
            ถึง 
            {{ App\Http\Controllers\FunctionController::DateThai($request['date_end']) }}
            @endif
        </p>
    </center>
    <br>
    {{-- <hr style="margin:0px;"> --}}
    <div style="font-size:16px !important;">
        <table>
            <tr>
                <td><strong>รหัสพนักงาน</strong>&nbsp;{{ isset($employee->empcode)?$employee->empcode:'' }}</td>
                <td><strong>ชื่อ</strong>&nbsp;{{ isset($employee->firstname)?$employee->firstname:'' }}</td>
                <td><strong>นามสกุล</strong>&nbsp;{{ isset($employee->lastname)?$employee->lastname:'' }}</td>
                <td rowspan="3">
                    <img class="pictureprofile" src="{{public_path($employee->picture_profile)}}"/>
                </td>
            </tr>
            <tr>
                <td><strong>บริษัท</strong>&nbsp;{{ isset($employee->cname)?$employee->cname:'' }}</td>
                <td><strong>สังกัด</strong>&nbsp;{{ isset($employee->bname)?$employee->bname:'' }}</td>
                <td><strong>ฝ่าย</strong>&nbsp;{{ isset($employee->gname)?$employee->gname:'' }}</td>
            </tr>
            <tr>
                <td><strong>ตำแหน่ง</strong>&nbsp;{{ isset($employee->lname)?$employee->lname:'' }}</td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    {{-- <hr style="margin:0px;"> --}}
    <br>
    <table class="datatable">
        <tr>

            <th>ลำดับ</th>
            <th>ประเภท</th>
            <th>วันที่มีผล</th>
            {{-- <th>สิ้นสุด</th> --}}
            <th>รายละเอียด</th>

        </tr>
        @foreach ($data as $key => $item)
            <tr>
                <td>{{ ($key+1 ) }}</td>
                <td>{{ $item->wname }}</td>
                <td>{{ $item->start_date }}</td>
                {{-- <td>{{ $item->end_date }}</td> --}}
                <td>{{ $item->detail }}</td>
            </tr>
        @endforeach
        @for ($i = 1; $i <= 10-count($data); $i++)
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                {{-- <td>&nbsp;</td> --}}
                <td>&nbsp;</td>
            </tr>
        @endfor
    </table>
<p style="float:right; margin:0px;" >ข้อมูลวันที่ : {{ App\Http\Controllers\FunctionController::DateThai(date('Y-m-d')) }} , เวลา {{date('H:i:s')}}</p>
</body>
</html>