@extends('admin.layouts.app')

@section('style')
@stop

@section('script')
<script src="{{asset('js/admin/install.js')}}"></script>
<script src="{{asset('assets/plugins/validate_th.js')}}"></script>
@stop

@section('content')
<div class="container-fixed-lg bg-white">
    <!-- START card -->
    <div class="card">
        <div class="card-header ">
            <div class="card-title">
                <h5>{{$menu}}</h5>
            </div>
        </div>
        <div class="card-body">
            <table id="{{$menu}}" class="table table-hover table-condensed dataTable" style="width:100%;">
                <col width="25%" />
                <col width="75%" />
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Table</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- END card -->
</div>
<form class="validateForm">
    <div class="column card d-none">

        <div class="card-header">
            <input type="hidden" name="table">
            <h5 id="tablename" class="pull-left"></h5>
            <button type="button" class="btn btn-danger btn-cons btn-removemvc pull-right">
                <i class="far fa-trash-alt"></i> mvc
            </button>
        </div>
        <div class="card-body table-responsive">
            <table id="column" class="table" style="width:100%;">
                <col width="10%" />
                <col width="20%" />
                <col width="15%" />
                <col width="10%" />
                <col width="15%" />
                <col width="15%" />
                <col width="15%" />
                <thead>
                    <tr>
                        <th>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="checkall">
                                <label class="form-check-label" for="checkall"></label>
                            </div>
                        </th>
                        <th>field</th>
                        <th>type</th>
                        <th>validate</th>
                        <th>input type</th>
                        <th>source</th>
                        <th>name/value</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="column card d-none">
        <div class="card-body">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
@stop
