@extends('admin.layouts.app')

@section('style')
<link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
@stop

@section('script')
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}"></script>

<script src="{{asset('assets/admin/js/admin/employeeconfirm.js')}}"></script>

<script>
	$('select[name="group"]').select2();
	$('select[name="department"]').select2();
	$('select[name="name"]').select2();
	$('select[name="status"]').select2();
	$('input[name="month"]').datepicker({
        format: 'yyyy-mm',
        autoclose: true,
        language: 'th',
        startView: "months", 
        minViewMode: "months"
	});
	
	$('#filter').submit(function (e) {
		console.log($('#filter').serialize());
		e.preventDefault();
		var new_option = {
			"responsive": true,
			"serverSide": true,
			"processing": true,
			"ajax": {
				"url": rurl + 'admin/employeeconfirm/list?'+$('#filter').serialize(),
				"type": "POST",
				"data": $('#filter').serialize()
			},
			"language": { "url" : rurl + "assets/plugins/datatable_th.json" },
			"columns": [
				{
					"data": 'DT_RowIndex',
					"name": 'DT_RowIndex',
					orderable: false,
					searchable: false,
					className:"text-center"
				},
				{"data":"empcode","name":"employee.empcode"},
				{"data":"employee_name" , "name":"employee.firstname"},
				{"data":"month","name":"employee_confirm.month"},
				{"data":"created_at","name":"employee_confirm.created_at"},
				{"data":"status","name":"employee_confirm.status"},
				{
					"data": "action",
					orderable: false,
					searchable: false
				},
				{"data":"firstname","name":"employee.firstname","visible": false},
				{"data":"lastname","name":"employee.lastname","visible": false}
			]
		}
		$('#employeeconfirm').DataTable().destroy();
        $('#employeeconfirm').DataTable(new_option);
	});

	$('.ls-select2').select2();
</script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col-12">
				<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
				<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
					+ {{ isset($menu) ? $menu : '' }}
				</button>
			</div>
		</div>

		<form method="post" id="filter">
		<div class="row">
			<div class="col-md-2 col-sm-4">
				<input class="form-control" name="month" value="{{date('Y-m')}}">
			</div>
			<div class="col-md-2 col-sm-4">
				<select class="ls-select2" name="group_id">
					<option value="">== ฝ่าย ==</option>
					@foreach ($groups as $item)
						<option value="{{$item->id}}">{{$item->name}}</option>
					@endforeach
				</select> 
			</div>
			<div class="col-md-2 col-sm-4">
				<select class="ls-select2" name="department_id">
					<option value="">== แผนก ==</option>
					@foreach ($departments as $item)
					<option value="{{$item->id}}">{{$item->name}}</option>
					@endforeach
				</select> 
			</div>
			<div class="col-md-2 col-sm-4">
				<select class="ls-select2" name="employee_id">
					<option value="">== ชื่อ-สกุล ==</option>
					@foreach ($employee as $item)
						<option value="{{$item->id}}">{{$item->firstname .' '. $item->lastname}}</option>
					@endforeach
				</select> 
			</div>
			<div class="col-md-2 col-sm-4">
				<select class="ls-select2" name="status">
					<option value="">== สถานะ ==</option>
					<option value="T">ยืนยัน</option>
					<option value="F">ไม่ยืนยัน</option>
				</select> 
			</div>
			<div class="col-md-2 col-sm-4">
				<button class="btn btn-sm btn-primary btn-block">ค้นหา</button>
			</div>
		</div>
		</form>
		
	</div>
	<div class="card-body">
		<table id="employeeconfirm" class="table table-xs table-hover table-bordered table-striped dataTable no-footer"
			cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>รหัส</th>
					<th>ชื่อ-สกุล</th>
					<th>เดือน</th>
					<th>ยืนยัน</th>
					<th>สถานะ</th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
						{{-- <p class="p-b-10"></p> --}}
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="work_day" class="col-sm-2 col-form-label">work_day</label>
							<div class="col-sm-10">
								<input type="text" name="work_day" placeholder="work_day" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="absence_day" class="col-sm-2 col-form-label">absence_day</label>
							<div class="col-sm-10">
								<input type="text" name="absence_day" placeholder="absence_day"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="late_day" class="col-sm-2 col-form-label">late_day</label>
							<div class="col-sm-10">
								<input type="text" name="late_day" placeholder="late_day" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="id" class="col-sm-2 col-form-label">id</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="employee_id">
									<option value="">== id ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{$item->firstname}}">{{$item->id}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="created_at" class="col-sm-2 col-form-label">created_at</label>
							<div class="col-sm-10">
								<input type="text" name="created_at" placeholder="created_at"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="total_day" class="col-sm-2 col-form-label">total_day</label>
							<div class="col-sm-10">
								<input type="text" name="total_day" placeholder="total_day"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="month" class="col-sm-2 col-form-label">month</label>
							<div class="col-sm-10">
								<input type="text" name="month" placeholder="month" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="status" class="col-sm-2 col-form-label">status</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="status">
									<option value="">== สถานะ ==</option>
									<option value="T"> ยืนยัน </option>
									<option value="F"> ไม่ยืนยัน </option>
								</select>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop