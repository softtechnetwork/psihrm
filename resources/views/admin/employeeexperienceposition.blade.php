@extends('admin.layouts.app')

@section('style')
<link rel="stylesheet" href="{{asset('template/condensed/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}">
@stop

@section('script')
<script src="{{asset('template/condensed/assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('template/condensed/assets/plugins/moment/moment-with-locales.min.js')}}"></script>
<script src="{{asset('template/condensed/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>

<script src="{{asset('assets/admin/js/admin/employeeexperienceposition.js')}}"></script>
<script>
	$('[name="start_date"]').daterangepicker({
        timePicker: false,
        timePickerIncrement: 30,
        format: 'YYYY-MM-DD',
        setDate:"{{date('Y-m-d')}}"
    },function(start, end, label){
        $('[name="start_date"]').val(start.format('YYYY-MM-DD'));
        $('[name="end_date"]').val(end.format('YYYY-MM-DD'));
    });
</script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<div class="card-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseHistory">
				<h5>ประวัติ{{ isset($menu) ? $menu : '' }}</h5>
			</a>
		</div>
	</div>
	<div class="card-body">
		<form id="employeeexperienceposition">
			<input type="hidden" name="experience_type" value="p">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<label for="leves">พนักงาน</label>
					<select class="form-control ls-select2" name="employee_id" id="employee_id" tabindex="-1" aria-hidden="true">
						<option value="">== พนักงาน ==</option>
						@foreach ($employee as $item)
						<option value="{{$item->id}}">{{ $item->prename.$item->firstname." ".$item->lastname }}</option>
						@endforeach
					</select>
				</div>

				<div class="col-md-4 col-sm-6">
					<label for="branch_id">สาขา</label>
					<select class="form-control ls-select2" name="branch_id" id="branch_id" tabindex="-1" aria-hidden="true">
						<option value="">== สาขา ==</option>
						@foreach ($branch as $item)
						<option value="{{$item->branch_id}}">{{ $item->branch_name }}</option>
						@endforeach
					</select>
				</div>

				<div class="col-md-4 col-sm-6">
					<label for="department_id">แผนก</label>
					<select class="form-control ls-select2" name="department_id" id="department_id" tabindex="-1" aria-hidden="true">
						<option value="">== แผนก ==</option>
						@foreach ($department as $item)
						<option value="{{$item->id}}">{{ $item->name }}</option>
						@endforeach
					</select>
				</div>

			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<label for="level_id">ตำแหน่ง</label>
					<select class="form-control ls-select2" name="level_id" id="level" tabindex="-1" aria-hidden="true">
						<option value="">== ตำแหน่ง ==</option>
						@foreach ($level as $item)
						<option value="{{$item->id}}">{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4 col-sm-6">
					<label for="date_start">ช่วงวันที่</label>
					<input type="text" name="start_date" id="date_start" class="form-control">
				</div>
				<div class="col-md-4 col-sm-6">
					<label for="date_end">ถึงวันที่</label>
					<input type="text" name="end_date" id="date_end" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<label for=""></label>
					<button type="submit" class="btn btn-primary btn-block">ค้นหา</button>
				</div>
				<div class="col-md-4 col-sm-6">
					<label for=""></label>
					<a class="btn btn-default btn-block btn-report-people">พิมพ์รายงาน</a>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="card">
	<div class="card-body" id="collapseHistory">
		<table id="employeeexperience"
			class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0"
			width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>ชื่อ</th>
					<th>ชื่อ</th>
					<th>นามสกุล</th>
					<th>ตำแหน่ง</th>
					<th>แผนก</th>
					<th>สาขา</th>
					<th>วันที่มีผล</th>
					{{-- <th>สิ้นสุด</th> --}}
				</tr>
			</thead>
		</table>
	</div>
</div>
@stop