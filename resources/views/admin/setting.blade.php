@extends('admin.layouts.app')

@section('script')
<script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script>
    $('#login_picture').filemanager('image');
    $('#icon').filemanager('image');
    $('input[name="icon"]').change(function (e) {
        e.preventDefault();
        $('img#holder_icon').attr('src',this.value);
    });
    $('input[name="login_picture"]').change(function (e) {
        e.preventDefault();
        $('img#holder_login_picture').attr('src',this.value);
    });
    $('.ls-select2').select2();
    $('form').submit(function (e) {
        var btn = $('button.btn');
        btn.prop('disabled', true);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: rurl + "admin/setting",
            data: $('form').serialize(),
            // dataType: "json",
            success: function (response) {
                swal('สำเร็จ', response, "success");
                btn.prop('disabled', false);
                console.log(response);
            }
        });
    });
    function set_gettting(){
        $.ajax({
            type: "GET",
            url: rurl + "admin/setting/get",
            // dataType: "json",
            success: function (response) {
                console.log(response);
                $.each(response, function (indexInArray, valueOfElement) {
                    if(valueOfElement.name=="assist_id"){
                        $("[name='"+valueOfElement.name+"[]']").val(JSON.parse(valueOfElement.value));
                        // $(".ls-select2").select2();
                    }else{
                        $("[name='"+valueOfElement.name+"']").val(valueOfElement.value);
                    }
                });
                $('input[name="login_picture"]').change();
                $('input[name="icon"]').change();
                $('.ls-select2').select2();
            }
        });
    }
    set_gettting();
</script>
@stop

@section('content')
<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">

        <div class="card card-default">
            
            <div class="card-body">
                <h4>ตั้งค่า</h4>
                <p class="m-t-10 m-b-20 mw-80">
                </p>
                <form method="POST" role="form">
                    {{-- {{ csrf_token() }} --}}
                    <div class="form-group">
                        <label>project name</label>
                        <input type="text" class="form-control" name="project_name" required="">
                    </div>

                    {{-- <div class="form-group">
                        <label>icon</label>
                        <div class="col-lg-6 text-center" style="padding:5px; border:1px dotted rgba(100,100,100,0.1); background:rgba(200,200,200,0.1);">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <a id="icon" data-input="thumbnail_icon" data-preview="holder_icon"
                                        class="btn btn-default">
                                        <i class="fa fa-picture-o"></i>
                                    </a>
                                </span>
                                <input id="thumbnail_icon" class="form-control" type="text" name="icon">
                            </div>
                            <img id="holder_icon" class="img-fluid  text-center"style="margin-top:5px; max-height:150px;">
                        </div>
                    </div> --}}

                    <div class="form-group">
                        <label>login picture</label>
                        <div class="col-lg-6 text-center" style="padding:5px; border:1px dotted rgba(100,100,100,0.1); background:rgba(200,200,200,0.1);">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <a id="login_picture" data-input="thumbnail_login_picture" data-preview="holder_login_picture"
                                        class="btn btn-defau">
                                        <i class="fa fa-picture-o"></i>
                                    </a>
                                </span>
                                <input id="thumbnail_login_picture" class="form-control" type="text" name="login_picture">
                            </div>
                            <img id="holder_login_picture" class="img-fluid  text-center"style="margin-top:5px; max-height:150px;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>CEO</label>
                        <select class="ls-select2 form-control" name="ceo_id" id="">
                            <option value="">== CEO ==</option>
                        @foreach ($employee as $item)
                            <option value="{{ $item->id }}">{{ $item->prename.$item->firstname.' '.$item->lastname }}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>ผู้ช่วยจัดการข้อมูล</label>
                        <select class="ls-select2 form-control" name="assist_id[]" multiple="multiple">
                            <option value="">== ผู้ช่วยจัดการข้อมูล ==</option>
                        @foreach ($employee as $item)
                            <option value="{{ $item->id }}">{{ $item->prename.$item->firstname.' '.$item->lastname }}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary btn-block pull-right">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
        

    </div>
    <div class="col-lg-2"></div>
</div>
@stop