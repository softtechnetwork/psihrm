<!-- BEGIN NAV -->
<nav class="page-sidebar" data-pages="sidebar">
    <div class="sidebar-overlay-slide from-top" id="appMenu"></div>
    <div class="sidebar-header">
        <img src="<?php echo e(asset('assets/img/logo-hrm.png')); ?>" alt="logo" class="brand"
            data-src="<?php echo e(asset('assets/img/logo-hrm.png')); ?>" data-src-retina="<?php echo e(asset('assets/img/logo-hrm_2x.png')); ?>"
            width="78" height="22" />
    </div>
    <div class="sidebar-menu">
        <ul class="menu-items">
            <?php if(\Auth::guard('admin')->check()): ?>
                <?php $current_url_id=null; ?>
                <?php
                foreach($adminmenu as $key => $value){
                    if( asset($value->menu_url) == url()->current() ){
                        $current_url_id = $value->main_menu;
                    }
                }
                ?>
                <?php $__currentLoopData = $adminmenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($item->main_menu == 0): ?>
                        <li class="<?php echo e(((asset($item->menu_url)==url()->current())?'active':'')); ?> <?php echo e($current_url_id==$item->id?'active':''); ?> <?php echo e($item->id==1?'m-t-30':''); ?>">
                            <a href=" <?php echo e(!empty($item->menu_url)? asset($item->menu_url) : '#'); ?>">
                                <span class="title"><?php echo e($item->menu_name); ?></span>
                                <?php if( ($item->main_menu==0) && empty($item->menu_url)): ?>
                                <span
                                    class="arrow <?php echo e(( (url(''.$item->menu_url)==url()->current())||$current_url_id==$item->id ? 'open':'')); ?>"></span>
                                <?php endif; ?>
                            </a>
                            <span class="icon-thumbnail">
                                <i class="fa <?php echo $item->menu_icon; ?>"></i>
                            </span>
                            <?php
                            $check = null;
                            foreach($adminmenu as $key => $itemin){
                                if($item->id == $itemin->main_menu){
                                    $item ? $check[]=$itemin : '' ;
                                }
                            }
                            $check = $check==null ? 0 : count($check);
                            ?>
                            <?php if($check!=0): ?><ul class="sub-menu"><?php endif; ?>
                            <?php $__currentLoopData = $adminmenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $itemin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($item->id == $itemin->main_menu && $itemin->main_menu!=0): ?>
                                <li class="<?php echo e((( url( ''.$itemin->menu_url )==url()->current() ) ? 'open active':'')); ?>">
                                    <a href="<?php echo e(asset($itemin->menu_url)); ?>"><?php echo e($itemin->menu_name); ?></a>
                                    <span class="icon-thumbnail">
                                        <i class="fa <?php echo $itemin->menu_icon; ?>"></i>
                                    </span>
                                </li>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if($check!=0): ?> </ul> <?php endif; ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>
<!-- END NAV --><?php /**PATH C:\xampp\htdocs\psi_hrm\resources\views/admin/layouts/nav.blade.php ENDPATH**/ ?>