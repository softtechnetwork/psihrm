<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\Employee;
use App\Models\Projectassign;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['employee'] = Employee::get();

        $data['menu'] = 'โปรเจค';
        return view('admin.project')->with($data);
    }

    public function get_project_assign($id){
        
        $result = Projectassign::select([
            'project_assign.*'
            ,'project.name'
            ,'employee.firstname'
            ,'employee.lastname'
            ,'employee.id as employeeid'
            ,'employee.picture_profile'
            ,\DB::raw('employee.firstname+\' \'+employee.lastname as ename')
        ])
        ->where('project_id',$id)
        ->leftjoin('project','project.id','project_assign.project_id')
        ->leftjoin('employee','project_assign.employee_id','employee.id')
        ->get();
        $data['data'] = $result;
        $data['employees'] = array();
        foreach ($result as $key => $value) {
            $data['employees'][] = (int)$value->employee_id;
        }
        return $data;
    }

    public function post_project_assign(Request $request){
        // project_id	"197"
        // assign_people	[ "30", "190", "203", "211" ]
        if(isset($request->project_id) && isset($request->assign_people)){


            \DB::beginTransaction();
            try {
                Projectassign::where('project_id',$request->project_id)
                ->whereNotIn('project_id',$request->assign_people)
                ->delete();
                $get_projectassign = Projectassign::where('project_id',$request->project_id)
                ->get();
                $result = array();
                foreach ($get_projectassign as $key => $value) {
                    $result[] = $value->employee_id;
                }
                $loops = array_diff($request->assign_people,$result); // resutl = different items
                $new_record = array();
                foreach($loops as $key => $item){
                    $insert['project_id'] = $request->project_id;
                    $insert['employee_id'] = $item;
                    $insert['created_at'] = date('Y-m-d');
                    $new_record[] = $insert;
                }
                if(Projectassign::insert($new_record)){
                    \DB::commit();
                    return "อัพเดทข้อมูลสำเร็จ";
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return "อัพเดทข้อมูลไม่สำเร็จ => ".$e->getMessage();
            }

        }
    }

    public function list(){
        $model = Project::query();
        $model->leftjoin('employee','project.created_by','employee.id');
        $model->select([
            'project.*','project.name as pname','project.id as projectid'
            ,'employee.firstname'
            ,'employee.lastname'
            ,'employee.id as employeeid'
            ,\DB::raw('employee.firstname+\' \'+employee.lastname as ename')
        ]);
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                        <a class="btn btn-xs btn-info btn-manage-people" href="#" data-id="'.$rec->projectid.'">
                            <i class="fa fa-user"></i>
                        </a>
                        <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->projectid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->projectid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->addColumn('assign',function($rec){
                    $query = Projectassign::select([
                        'employee.firstname',
                        'employee.lastname',
                        'employee.prename'
                    ])
                    ->leftjoin('employee','employee.id','project_assign.employee_id')
                    ->where('project_id',$rec->id)
                    ->get();
                    $str = "";
                    foreach ($query as $key => $item) {
                        $str .= $item->prename.$item->firstname.' '.$item->lastname.'</br>';
                    }
                    return $str;
                })
                ->addIndexColumn()
                ->rawColumns(['action','assign'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Project::insert($request->all())){
                    \DB::commit();
                    return "บันทึกสำเร็จ";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Project::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            if($result = Project::where('id',$id)->update($request->all())){
                \DB::commit();
                return "อัพเดทข้อมูลสำเร็จ";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Project::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "ลบข้อมูลสำเร็จ";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}