<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Evaluation;
use App\Models\Evaluationtype;
use App\Models\Structurepivot;
use App\Models\Evaluationresult;
use App\Models\EvaluationCalculateLog;
use App\Http\Controllers\Admin\EvaluationtypeController;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\FunctionController as FC;

use stdClass;

class SpecialFunctionController extends Controller // App\Http\Controllers\SpecialFunctionController
{
    public function get_structure($employee_id=null,$evaluation_id=null){
        $query = Employee::select([
            'employee.id as employee_id'
            ,\DB::raw("'".url('/')."'+employee.picture_profile as picture_profile")
            ,'employee.empcode as employee_code'
            ,\DB::raw("employee.firstname+' '+employee.lastname as name")
            ,'employee_level.name as employee_level_name'
            ,'groups.name as group_name'
            ,'department.name as department_name'
            ,'level.name as position_name'
            ,'branch.branch_name'
            ,\DB::raw("
                (SELECT TOP 1 eo.prename+eo.firstname+' '+eo.lastname FROM employee eo WHERE eo.id = (SELECT s.employee_id FROM structure_pivot s WHERE s.box_id = structure_pivot.box_top_id)) as leader_name
            ")
        ]);
        $query->leftjoin('employee_level', 'employee.employee_level_id', 'employee_level.id');
        $query->leftjoin('groups', 'employee.group_id', 'groups.id');
        $query->leftjoin('department', 'employee.department_id', 'department.id');
        $query->leftjoin('level', 'employee.level_id', 'level.id');
        $query->leftjoin('branch', 'employee.branch_id', 'branch.id');
        $query->leftjoin('structure_pivot', 'structure_pivot.employee_id', 'employee.id');
        $query->orderBy('employee.id', 'ASC');
        $query->active();
        $results = $query->get();
        return $results;
    }


    public function query_get_value($employee_id=211, $evaluation_id=19)
    {
        $add_field = '';
        $add_where = '';

        if (isset($evaluation_id) && $evaluation_id!='all'){
            $table = 'structure_evaluation';
            $add_where  .= " AND er.evaluation_id = $evaluation_id";
        } else {
            $table = 'structure_pivot';
            $evaluation_id = Evaluation::latest('id')->first()->id;
        }
        
        // ฟิลด์ข้อมูล AVG การประเมิน
        $add_field .=
        'sp.box_id
        ,sp.box_top_id
        ,sp.employee_id
        ,( SELECT count(DISTINCT spi.employee_id) FROM '.$table.' spi WHERE spi.box_top_id = sp.box_id )as child';
        $evalaution_type = EvaluationType::active()->get();
        foreach ($evalaution_type as $key => $value) {
            $add_field .=
            ',ROUND( AVG( CASE WHEN (er.evaluation_type_id = '.$value->id.' AND er.score>0 ) THEN er.score ELSE NULL END ) , 2 ) AS '.(str_replace(' ', '_', strtolower(trim($value->name))));
        }

        $str_query =
        "
            SELECT
                $add_field
            FROM
                $table sp
                LEFT JOIN evaluation_result er ON er.employee_target_id = sp.employee_id
                LEFT JOIN employee e ON e.id = er.employee_target_id
            WHERE
                ( sp.box_id LIKE ( SELECT TOP 1 s.box_id FROM $table s WHERE s.employee_id = $employee_id )+'%' )
                $add_where
            GROUP BY
                sp.box_id,
                sp.box_top_id,
                sp.employee_id
            ORDER BY sp.box_top_id ASC
        ";

        $result = \DB::select($str_query);

        $data = array();
        Storage::disk('app')->delete("attitude.txt");
        Storage::disk('app')->delete("job_description.txt");
        Storage::disk('app')->delete("kpi.txt");
        // foreach ($evalaution_type as $k => $v) {
        //     $score = $this->recursive_evaluation_score($result, $employee_id, (str_replace(' ', '_', strtolower(trim($v->name)))));
        //     $obj = new stdClass();
        //     $obj->id = (string) $v->id;
        //     $obj->name = $v->name;
        //     $obj->ratio = $v->ratio;
        //     $obj->score = FC::star($score);
        //     $obj->score_float = $score;
        //     $obj->grade = FC::grade($score);
        //     $data[] = $obj;
        // }
        // return $data;
        return ($this->recursive($result));
    }

    public function recursive($obj,$box_top_id=null){
        $employies = array();
        foreach($obj as $key => $value){
            if($value->box_top_id==""){
                unset($obj[$key]);
                if((int)$value->child>0){
                    $recursive = $this->recursive($obj,$value->box_id);
                    if(!empty($recursive)){ $employies[] = $recursive; }
                }
                $employies[] = ROUND((float)$value->kpi,2);
                Storage::disk('app')->append("kpi.txt",'['.$value->employee_id.']|'.json_encode($employies));
            }else if($value->box_top_id===$box_top_id && $value->box_top_id!=""){
                unset($obj[$key]);
                if((int)$value->child>0){
                    $recursive = $this->recursive($obj,$value->box_id);
                    if(!empty($recursive)){ $employies[] = $recursive; }
                }
                $employies[] = ROUND((float)$value->kpi,2);
                Storage::disk('app')->append("kpi.txt",'['.$value->employee_id.']|'.json_encode($employies));
            }
        }
        return $employies;
    }

    public function recursive_binary_tree($obj,$box_top_id=null){
        $employies = array();
        foreach($obj as $key => $value){
            $val = $value;
            if($value->box_top_id==""){
                unset($obj[$key]);
                if((int)$value->child>0){
                    $val->child = $this->recursive($obj,$value->box_id);
                }
                $employies[] = $val;
                Storage::disk('app')->append("kpi.txt",json_encode($val));
            }elseif($value->box_top_id==$box_top_id){
                unset($obj[$key]);
                if((int)$value->child>0){
                    $val->child = $this->recursive($obj,$value->box_id);
                }
                $employies[] = $val;
                Storage::disk('app')->append("kpi.txt",json_encode($val));
            }
        }
        return $employies;
    }

    public function recursive_evaluation_score($obj, $employee_id=null, $evaluation_type_name)
    {
        $parameter = $evaluation_type_name;
        $items = $obj;
        $array = [];
        $check_top_box_id = null;

        foreach ($items as $key => $item) {
            if ($employee_id==$item->employee_id) {
                $check_top_box_id = $item->box_id;
                $array[] = (float)($item->{$parameter});
                Storage::disk('app')->prepend($evaluation_type_name.".txt", json_encode((float)($item->{$parameter})) );
                unset($items[$key]);
                break;
            }
        }

        $array_in = array();
        foreach ($items as $key => $item) {
            if ($check_top_box_id==$item->box_top_id) {
                if ((int)($item->child)!=0) {
                    $fetchs = EvaluationtypeController::recursive_evaluation_score($items, (int)($item->employee_id), $parameter);
                    if (is_array($fetchs)) {
                        $cal = (count(array_filter($fetchs))==0) ? array_sum(array_filter($fetchs)) : array_sum(array_filter($fetchs)) / count(array_filter($fetchs));
                        $array_in[] = $cal;
                    } else {
                        $array_in[] = $fetchs;
                    }
                } else {
                    $cal = (float)($item->{$parameter});
                    $array_in[] = $cal;
                }
                unset($items[$key]);
            }
        }

        if (!empty($array_in)) {
            $cal = (count(array_filter($array_in))==0) ? array_sum(array_filter($array_in)) : array_sum(array_filter($array_in)) / count(array_filter($array_in));
            $array[] = $cal;
        }
        if (!empty($array)) {
            $return = (count(array_filter($array))==0) ? array_sum(array_filter($array)) : array_sum(array_filter($array)) / count(array_filter($array));
        }

        $return = isset($return) ? $return : 0 ;

        return $return;
    }
}
