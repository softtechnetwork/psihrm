<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workdaygroup extends Model
{
    protected $table = 'workday_group';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}