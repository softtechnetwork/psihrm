<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Structurepivot extends Model
{
    protected $table = 'structure_pivot';
    public $timestamps = true;
}
