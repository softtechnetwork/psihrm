<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    protected $table = 'groups';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}