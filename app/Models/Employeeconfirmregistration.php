<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeconfirmregistration extends Model
{
    protected $table = 'employee_confirm_registration';
    public $timestamps = true;
}