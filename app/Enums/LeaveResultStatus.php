<?php

namespace App\Enums;

class LeaveResultStatus
{
    const APPROVE = 'T';
    const UN_APPROVE = 'N';
    const NOT_APPROVE = 'F';
}