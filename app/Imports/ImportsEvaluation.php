<?php

namespace App\Imports;

use App\Models\Importevaluation;
use Maatwebsite\Excel\Concerns\ToModel;
Use Maatwebsite\Excel\Concerns\WithStartRow;

class ImportsEvaluation implements ToModel , WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Importevaluation([
            'employee' => $row[0],
            'employee_target' => $row[1],
            'job' => $row[2],
            'kpi' => $row[3],
            'attitute' => $row[4],
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }
    
    public function chunkSize(): int
    {
        return 1000;
    }

    public function startRow() : int
    {
        return 2;
    }
}